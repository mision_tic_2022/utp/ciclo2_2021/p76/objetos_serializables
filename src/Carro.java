import java.io.Serializable;

public class Carro implements Serializable {
    /************
     * Atributos
     ***********/
    private String placa;
    private String color;
    private String fabricante;
    private int caballos_fuerza;

    /*************
     * Constructor
     *************/
    public Carro(String placa, String color, String fabricante, int caballos_fuerza) {
        this.placa = placa;
        this.color = color;
        this.fabricante = fabricante;
        this.caballos_fuerza = caballos_fuerza;
    }

    /***********************
     * Getters(Consultores) and Setters(Modificadores)
     * *********************/

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getFabricante() {
        return fabricante;
    }

    public void setFabricante(String fabricante) {
        this.fabricante = fabricante;
    }

    public int getCaballos_fuerza() {
        return caballos_fuerza;
    }

    public void setCaballos_fuerza(int caballos_fuerza) {
        this.caballos_fuerza = caballos_fuerza;
    }

}